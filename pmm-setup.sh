#!/bin/bash
######################################################
# PMM for centminmod.com on CentOS 7
# https://www.percona.com/doc/percona-monitoring-and-management/index.html
# written by George Liu (eva2000) centminmod.com
######################################################
# variables
#############
DT=`date +"%d%m%y-%H%M%S"`
DIR_TMP='/svr-setup'

PMMVER='2'
PMM_SERVERNAME='pmm-server'
PMM_DATANAME='pmm-data'
PMM_LOCALPORT='8180'
######################################################
# functions
#############

docker_setup() {
  yum -y install docker
  sed -i "s|OPTIONS='--selinux-enabled --log-driver=journald'|OPTIONS='--selinux-enabled=false --log-driver=journald -g /home/dockerdisk'|" /etc/sysconfig/docker
  # sed -i 's|DOCKER_STORAGE_OPTIONS= .*|DOCKER_STORAGE_OPTIONS= -s overlay|' /etc/sysconfig/docker-storage
  echo "STORAGE_DRIVER=overlay" > /etc/sysconfig/docker-storage-setup
  service docker start
  chkconfig docker on
  docker version
  docker info
  uname -a
}

pmm_serversetup() {
  docker create -v /srv --name "$PMM_DATANAME" percona/pmm-server:${PMMVER} /bin/true
  service docker restart

  # PMM Index 127.0.0.1:8180
  # Query Analytics 127.0.0.1:8180/qan/
  # Metrics Monitor 127.0.0.1:8180/graph/

  # whitelist
  if [[ -f /etc/csf/csf.conf && -z "$(grep "$PMM_LOCALPORT," /etc/csf/csf.conf)" ]]; then
    sed -i "s/TCP_IN = \"/TCP_IN = \"$PMM_LOCALPORT,/g" /etc/csf/csf.conf
    sed -i "s/TCP6_IN = \"/TCP6_IN = \"$PMM_LOCALPORT,/g" /etc/csf/csf.conf
    csf -r >/dev/null 2>&1
  fi

  # assign max of 50% of system available memory to prometheus
  # so limit needs to be set to 60% of that 50% so assign a limit 
  # of 30% of available memory
  memavail=$(awk '/MemAvailable/ {print $2}' /proc/meminfo)
  memlimit=$((($memavail*30)/100))
  
  # set pmm server interface user/password
  pmmuser="pmm$(pwgen -1cnys 11)"
  pmmpass=$(pwgen -1cnys 31)
  touch /etc/centminmod/pmm-server/login-user.ini
  touch /etc/centminmod/pmm-server/login-pass.ini
  echo $pmmuser >> /etc/centminmod/pmm-server/login-user.ini
  echo $pmmpass >> /etc/centminmod/pmm-server/login-pass.ini

  docker run -d -p "$PMM_LOCALPORT:80" --volumes-from "$PMM_DATANAME" --name "$PMM_SERVERNAME" --restart always \
    -e DISABLE_TELEMETRY=true \
    -e METRICS_MEMORY=$memlimit \
    -e METRICS_RETENTION=720h \
    -e QUERIES_RETENTION=30 \
    -e SERVER_USER=$pmmuser \
    -e SERVER_PASSWORD=$pmmpass \
    percona/pmm-server:${PMMVER}
  
  docker ps -a
  docker exec -it pmm-server head -1 /srv/update/main.yml
}

pmm_serverdel() {
  docker stop "$PMM_SERVERNAME" && docker rm "$PMM_SERVERNAME"
  echo
  read -ep "remove "$PMM_DATANAME" container ? [y/n] " rmdata
  if [[ "$rmdata" = [yY] ]]; then
    docker rm "$PMM_DATANAME"
  fi
}

pmm_serverupdate() {
  docker stop "$PMM_SERVERNAME" && docker rm "$PMM_SERVERNAME"
  docker run -d -p "$PMM_LOCALPORT:80" --volumes-from "$PMM_DATANAME" --name "$PMM_SERVERNAME" --restart always percona/pmm-server:${PMMVER}
}

pmm_clientsetup() {
  cd "$DIR_TMP"
  wget -4 https://www.percona.com/downloads/pmm2/2.2.0/binary/redhat/7/x86_64/pmm2-client-2.2.0-6.el7.x86_64.rpm -O pmm2-client-2.2.0-6.el7.x86_64.rpm
  yum -y localinstall pmm2-client-2.2.0-6.el7.x86_64.rpm
  pmm-admin config --server-insecure-tls --server-url="https://127.0.0.1:$PMM_LOCALPORT"
  echo
  echo "------------------------------------"
  echo "  pmm-admin add os
  cat /usr/local/percona/pmm.yml"
  pmm-admin add os
  cat /usr/local/percona/pmm.yml
  echo
  echo "------------------------------------"
  echo "pmm-admin add queries"
  pmm-admin add queries
  echo
  echo "------------------------------------"
  echo "pmm-admin add mysql"
  pmm-admin add mysql
  echo
  echo "------------------------------------"
  echo "pmm-admin list"
  pmm-admin list
  echo
  echo "------------------------------------"
  echo "pmm-admin check-network"
  pmm-admin check-network
  echo
  echo "------------------------------------"
  echo "pmm-admin ping"
  pmm-admin ping
}

pmm_clientdel() {
  yum -y remove pmm2-client
}

listports() {
  echo
  echo "--------------------------------------------------------------------"
  netstat -plunt | egrep 'percona|mysqld_|node_exporter|docker'
  echo "--------------------------------------------------------------------"
}

listlogs() {
  echo
  echo "--------------------------------------------------------------------"
  ls -lAhrt /var/log | grep 'pmm\-'
  echo "--------------------------------------------------------------------"
}

######################################################
case "$1" in
  setup )
  docker_setup
  pmm_serversetup
  pmm_clientsetup
  listports
  listlogs
    ;;
  update )
  pmm_serverupdate
  pmm_serverdel
  pmm_clientsetup
    ;;
  uninstall )
  pmm_serverdel
  pmm_clientdel
    ;;
  pattern )
    ;;
  pattern )
    ;;
  * )
    echo "$0 {setup|update|uninstall}"
    ;;
esac